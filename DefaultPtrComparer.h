#pragma once

#include "IComparer.h"

template<class TYPE>
class CDefaultPtrComparer : public IComparer<TYPE>
{
public:
	CDefaultPtrComparer() {}
	virtual ~CDefaultPtrComparer() {}

	bool IsABigger() const
	{
		return (*m_cmpA > *m_cmpB);
	}
	bool IsBBigger() const
	{
		return (*m_cmpB > *m_cmpA);
	}
	bool Equal() const
	{
		return (*m_cmpA == *m_cmpB);
	}
};
