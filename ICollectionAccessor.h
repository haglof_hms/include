#pragma once

template<class TYPE>
class ICollectionAccessor
{
public:
	virtual TYPE GetAt(long nIndex) = 0;
	virtual void Swap(long nFirst, long nSecond) = 0;
	virtual long GetCount() = 0;
};