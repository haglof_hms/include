#include "StdAfx.h"
#include "XMLHandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// class XMLHandler

inline void CHECK( HRESULT _hr ) 
{ 
  if FAILED(_hr) throw(_hr); 
}

XMLHandler::XMLHandler()
{
	CHECK(CoInitialize(NULL));
  m_sModulePath	= _T("");
  pXMLDoc = NULL;
	m_sLangSet = getLangSet();
	m_bIsThisShortcuts = FALSE;
}

XMLHandler::XMLHandler(LPCTSTR module_path,HINSTANCE hinst)
{
	CHECK(CoInitialize(NULL));
  m_sModulePath	  = (module_path);
  pXMLDoc		  = NULL;
}

XMLHandler::~XMLHandler()
{
  CoUninitialize();
  m_sModulePath	  = _T("");
  pXMLDoc = NULL;
}

BOOL XMLHandler::load(LPCTSTR xml_fn)
{
	// Error handling added; 090319 Peter
	TRY
	{
		// Create MSXML2 DOM Document
		CHECK(pXMLDoc.CreateInstance("Msxml2.DOMDocument.3.0"));

		// Set parser property settings
		pXMLDoc->async = VARIANT_FALSE;

		// Validate during parsing
		pXMLDoc->validateOnParse = VARIANT_FALSE;

		// Load the sample XML file
		if (pXMLDoc->load(xml_fn) != VARIANT_TRUE)
		{
			BSTR pBReason;
			CString str;

			pXMLDoc->GetparseError()->get_reason(&pBReason);

			str.Format(_T("XMLHandler::load Error.\n%s\n%s"), pBReason, xml_fn);
			AfxMessageBox(str, MB_ICONEXCLAMATION | MB_OK);
			::SysFreeString(pBReason);

			return false;
		}
	}
	CATCH_ALL(e)
	{
		TCHAR szErr[256];
		e->GetErrorMessage(szErr, sizeof(szErr) / sizeof(TCHAR));
		AfxMessageBox(szErr);
		return false;
	}
	END_CATCH_ALL

	m_sModulePath = xml_fn;
	m_sLangSet = getLangSet();

	return true;
}

BOOL XMLHandler::save(LPCTSTR xml_fn)
{
	return pXMLDoc->save(xml_fn);
}

int XMLHandler::getVersionNumber(void)
{
	CComBSTR bstrData;
	TCHAR szData[32];
	MSXML2::IXMLDOMElementPtr pRoot = pXMLDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(MAIN_VERSION);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(szData,_bstr_t(bstrData));

		
		return _tstoi(szData);
	}

	return -1;
}

// Reads the Shell tree data and set the Shell tree in HFISShell program; 050324 p�d
BOOL XMLHandler::getShellTreeData(vecTreeList &tree_list,BOOL clr)
{
	HINSTANCE hModule = NULL;

	if (IsInList(m_sModulePath,tree_list)) 
		return FALSE;

	m_vecTreeList_temp.clear();
	// display the current node's name
	// simple for loop to get all children
	MSXML2::IXMLDOMNodeListPtr pNodeList = pXMLDoc->getElementsByTagName(TREE_TAG);
	if (pNodeList != NULL)
	{
		// Only clear list if clr = TRUE; 051115 p�d
		if (clr)
			tree_list.clear();

		m_nCounter = 0;

	  for (long i = 0;i < pNodeList->length;i++)
	  {

			MSXML2::IXMLDOMNodePtr pChild = pNodeList->item[i];

			interpretShellData(pChild);

	  }	// for (long i = 0;i < pNodeList->length;i++)

		for (UINT u = 0;u < m_vecTreeList_temp.size();u++)
			tree_list.push_back(CTreeList(m_vecTreeList_temp[u]));
									//CTreeList(sCaption,sIcon,sNodeName,sText,nVisible,nIndex,sAttr_Func,sAttr_Suite,
									//					nStrID,m_sModulePath,nCheck,i+1,sAttr_Caption));

	} // if (pNodeList != NULL)
	
	return TRUE;
}

BOOL XMLHandler::getSuitesInShellTree(CStringArray &suites_list)
{
	TCHAR szPath[MAX_PATH];
	TCHAR szNodeName[50];
	TCHAR szUserModule[50];
	// display the current node's name
	// simple for loop to get all children
	MSXML2::IXMLDOMNodeListPtr pNodeList = pXMLDoc->getElementsByTagName(SUITES_TAG_WC);
	if (pNodeList != NULL)
	{
		// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
		// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
		memset(szNodeName, 0, sizeof(szNodeName));
		memset(szUserModule, 0, sizeof(szUserModule));
		memset(szPath, 0, sizeof(szPath));
		// Clear um_list; 051130 p�d
		suites_list.RemoveAll();
		for (long i = 0;i < pNodeList->length;i++)
	  {
			MSXML2::IXMLDOMNodePtr pChild = pNodeList->item[i];
			_tcscpy(szUserModule,pChild->text);
			_tcscpy(szNodeName,pChild->nodeName);
			if (_tcscmp(szUserModule,_T("")) != 0)
			{
				// Remember to add Searchpath to suites xml files; 060612 p�d
				_stprintf(szPath,_T("%s\\%s"),getShellDataDir(),szUserModule);

				suites_list.Add(szPath);
			}	// if (_tcscmp(szUserModule,"") != 0)
		}	// for (long i = 0;i < pNodeList->length;i++)
	} // if (pNodeList != NULL)
	
	return TRUE;
}

#ifdef SUITEADMINISTRATION

BOOL XMLHandler::setSuitesInShellTree(CStringArray &suites_list)
{
	TCHAR szUserModule[50];
	CString S;
	// display the current node's name
	// simple for loop to get all children
	MSXML2::IXMLDOMNodeListPtr pNodeList = pXMLDoc->getElementsByTagName(SUITES_TAG_WC);
	if (pNodeList != NULL)
	{
	  szUserModule[0]	= '\0';
		for (long i = 0;i < pNodeList->length;i++)
	  {
			MSXML2::IXMLDOMNodePtr pChild = pNodeList->item[i];
			if (pChild)
			{
				if (i < suites_list.GetCount())
				{
					_stprintf(szUserModule,_T("%s%s"),suites_list[i],SHELLDATA_FN_EXT);
					pChild->text = szUserModule;
				} // if (i < suites_list.GetCount())
			} // if (pChild)
		}	// for (long i = 0;i < pNodeList->length;i++)
	} // if (pNodeList != NULL)
	
	return TRUE;
}


// Reads the Shell tree data and set the Shell tree in HFISShell program; 050324 p�d
BOOL XMLHandler::getShellTreeInfo(vecShellTree &tree_list)
{
	TCHAR sCaption[50];
	// Only clear list if clr = TRUE; 051115 p�d
	_tcscpy(sCaption,strid(SHELLTREE_ID,EXEC_ATTR5,EXEC_ATTR6));
	tree_list.push_back(CShellTreeInfo(sCaption,m_sModulePath));

	return TRUE;
}
#endif

CString XMLHandler::str(LPCTSTR path)
{
  CString szValue = _T("");
  BSTR szElem = NULL;
  if (pXMLDoc != NULL)
  {
	MSXML2::IXMLDOMElementPtr pRootNode = pXMLDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pChild = pRootNode->selectSingleNode(path);
	if (pChild != NULL)
	{
	  szElem = _bstr_t(pChild->text);
	  szValue = szElem;
	  return szValue;
	}
  }
  return _T("n/a");

} 


CString XMLHandler::strid(LPCTSTR path,LPCTSTR attr1,LPCTSTR attr2)
{
  MSXML2::IXMLDOMNamedNodeMapPtr pAttributes = NULL;
  MSXML2::IXMLDOMNodePtr pAttr = NULL;
  TCHAR szAttr[127];
  CString szValue1 = _T("");	// by Index
  CString szValue2 = _T("");	// by Caption
	int nStrID;
  if (pXMLDoc != NULL)
  {
		// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
		// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
		memset(szAttr, 0, sizeof(szAttr));
	
		MSXML2::IXMLDOMElementPtr pRootNode = pXMLDoc->documentElement;
		MSXML2::IXMLDOMNodePtr pChild = pRootNode->selectSingleNode(path);
		if (pChild != NULL)
		{
			// Get Caption
			pAttributes = pChild->attributes;
			pAttr = pAttributes->getNamedItem(_bstr_t(attr2));
			if (pAttr != NULL)
			{	
				_tcscpy(szAttr,pAttr->text);
				szValue2 = szAttr;
			}
			// Get id from index
			pAttr = pAttributes->getNamedItem(_bstr_t(attr1));
			if (pAttr != NULL)
			{	
				_tcscpy(szAttr,pAttr->text);
				nStrID = _tstoi(szAttr);
				szValue1 = getResStr(nStrID);
			}
			if (_tcscmp(szValue2,_T("")) != 0)
				return szValue2;
			else
				return szValue1;
		}
  }
  return _T("n/a");
} 

BOOL XMLHandler::setVisible(long number,BOOL show)
{
	MSXML2::IXMLDOMNodePtr pChild = NULL;
  MSXML2::IXMLDOMNamedNodeMapPtr pAttributes = NULL;
  MSXML2::IXMLDOMNodePtr pAttr = NULL;
	int nCaseOfShowHide = -1;
	CString S;
  if (pXMLDoc != NULL)
  {
		MSXML2::IXMLDOMElementPtr pRootNode = pXMLDoc->documentElement;
		MSXML2::IXMLDOMNodeListPtr pChildList = pRootNode->getElementsByTagName(_bstr_t(TREE_TAG));

		if (pChildList != NULL)
		{
			m_nFindNodeNum = number;
			m_bShowItem = show;
			m_nFindNodeNumCounter = 0;
			for (int i = 0;i < pChildList->length;i++)
			{
				MSXML2::IXMLDOMNodePtr pChild = pChildList->item[i];
				if (pChild)
				{
					findNodeByCounter(pChild);
				} // if (pChild)
			}
			return TRUE;
		}	// if (pChildList != NULL && number <= pChildList->length)

  }	// if (pXMLDoc != NULL)
  return FALSE;
}

BOOL XMLHandler::addMainItem(long number,CTreeList& rec,BOOL add_to_suite)
{
	TCHAR szItem[128];
	MSXML2::IXMLDOMNodePtr pChild = NULL;
  MSXML2::IXMLDOMAttributePtr pAttrib = NULL;

	// For now, items can only be added to Shortcuts.xml; 060404 p�d
	if (!add_to_suite) 
		return FALSE;

  if (pXMLDoc != NULL)
  {
		MSXML2::IXMLDOMElementPtr pRootNode = pXMLDoc->documentElement;
		MSXML2::IXMLDOMNodeListPtr pChildList = pRootNode->getElementsByTagName(TREE_TAG);
		if (pChildList != NULL && number > pChildList->length)
		{
				pChild = pRootNode->selectSingleNode(SHELLTREE_ID);
				if (pChild)
				{
					// Create node element
					MSXML2::IXMLDOMElementPtr pElem = pXMLDoc->createElement(rec.getNodeName());
					// Create and add attributes
					// Attribute: Visible
					pAttrib = pXMLDoc->createAttribute( EXEC_ATTR1 );	
					pAttrib->text = _T("1");
					pElem->attributes->setNamedItem( pAttrib );

					// Attribute: StrID
					pAttrib = pXMLDoc->createAttribute( EXEC_ATTR5 );	
					// Clear TCHAR items (NOT nders Gustafsson dubugging session 070305); 070306 p�d
					// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
					memset(szItem, 0, sizeof(szItem));
					_stprintf(szItem,_T("%d"),rec.getStrID());
					pAttrib->text = (szItem);
					pElem->attributes->setNamedItem( pAttrib );

					// Attribute: Caption (ItemText)
					pAttrib = pXMLDoc->createAttribute( EXEC_ATTR6 );	
					pAttrib->text = (rec.getItemText());
					pElem->attributes->setNamedItem( pAttrib );

					pChild->appendChild(pElem);
				} // if (pChild)
		}	// if (pChildList != NULL && number <= pChildList->length)
  }	// if (pXMLDoc != NULL)
  return FALSE;
}

BOOL XMLHandler::updMainItem(long number, CTreeList& rec,BOOL do_update)
{
  long i;
	TCHAR szItem[128];
	MSXML2::IXMLDOMNodePtr pChild = NULL;
  MSXML2::IXMLDOMNamedNodeMapPtr pAttributes = NULL;
  MSXML2::IXMLDOMAttributePtr pAttrib = NULL;

	// For now, items can only be added to Shortcuts.xml; 060404 p�d
	if (!do_update) 
		return FALSE;

  if (pXMLDoc != NULL)
  {
		MSXML2::IXMLDOMElementPtr pRootNode = pXMLDoc->documentElement;
		MSXML2::IXMLDOMNodeListPtr pChildList = pRootNode->getElementsByTagName(TREE_TAG);
		if (pChildList != NULL && number <= pChildList->length)
		{
			for (i = 0;i < pChildList->length;i++)
			{
				pChild = pChildList->item[i];

				if (pChild != NULL && i == (number-1))
				{
					// Create node element
					// Create and add attributes
					// Attribute: Visible
					pAttributes = pChild->attributes;
					pAttrib = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR1));
					if (pAttrib)
					{
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%d"),rec.getVisible());
						pAttrib->text = (szItem);
					}	// if (pAttrib)
					// Attribute: StrID
					pAttrib = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR5));
					if (pAttrib)
					{
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%d"),rec.getStrID());
						pAttrib->text = (szItem);
					}	// if (pAttrib)
					// Attribute: Caption (ItemText)
					pAttrib = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR6));
					if (pAttrib)
					{
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%s"),rec.getItemText());
						pAttrib->text = (szItem);
					}	// if (pAttrib)
				} // if (pChild)
			}	// for (i = 0;i < pChildList->length;i++)
		}	// if (pChildList != NULL && number <= pChildList->length)
  }	// if (pXMLDoc != NULL)
  return FALSE;
}

BOOL XMLHandler::delMainItem(vecTreeList& vec_orig,vecTreeList& vec_new,BOOL do_delete)
{
	CTreeList rec_orig;
	CTreeList rec_new;
	BOOL bFoundIt = FALSE;
	// Check if there's something to delete; 060504 p�d
	if (vec_orig.size() > vec_new.size() && do_delete)
	{
		// Try to find out which item's been deleted; 060504 p�d
		for (UINT cnt_orig = 0;cnt_orig < vec_orig.size();cnt_orig++)
		{
			bFoundIt = FALSE;
			rec_orig = vec_orig[cnt_orig];
			for (UINT cnt_new = 0;cnt_new < vec_new.size();cnt_new++)
			{
				rec_new = vec_new[cnt_new];
				if (rec_new.getCounter() == rec_orig.getCounter())
				{
					bFoundIt = TRUE;
					break;
				}	// if (rec_new.getCounter() == rec_orig.getCounter())
			}	// for (UINT cnt_new = 0;cnt_new < vec_new.size();cnt_new++)
			if (!bFoundIt)
			{
				delChild(rec_orig.getCounter());
			}
		}	// for (UINT cnt_orig = 0;cnt_orig < vec_orig.size();cnt_orig++)
		return TRUE;
	}
	return FALSE;
}

// PRIVATE
BOOL XMLHandler::delChild(long number)
{
	MSXML2::IXMLDOMNodePtr pChild = NULL;

  if (pXMLDoc != NULL)
  {
		MSXML2::IXMLDOMElementPtr pRootNode = pXMLDoc->documentElement;
		MSXML2::IXMLDOMNodeListPtr pChildList = pRootNode->getElementsByTagName(TREE_TAG);
		if (pChildList != NULL && number <= pChildList->length)
		{
			pChild = pChildList->item[number-1];

			if (pChild != NULL)
			{
				pRootNode->selectSingleNode(SHELLTREE_ID)->removeChild(pChild);
			} // if (pChild)
		}	// if (pChildList != NULL && number <= pChildList->length)
  }	// if (pXMLDoc != NULL)
  return FALSE;
}

BOOL XMLHandler::getSelectedItemInXMLFile(int number,CString& item_to_add)
{
	long i;
	TCHAR szItem[128];
	MSXML2::IXMLDOMNodePtr pChild = NULL;
  MSXML2::IXMLDOMNamedNodeMapPtr pAttributes = NULL;
  MSXML2::IXMLDOMNodePtr pAttr = NULL;
	int nCaseOfShowHide = -1;
  if (pXMLDoc != NULL)
  {
		MSXML2::IXMLDOMElementPtr pRootNode = pXMLDoc->documentElement;
		MSXML2::IXMLDOMNodeListPtr pChildList = pRootNode->getElementsByTagName(TREE_TAG);
		if (pChildList != NULL && number <= pChildList->length)
		{
			for (i = 0;i < pChildList->length;i++)
			{
				pChild = pChildList->item[i];
				if (pChild)
				{
					if (i == (number-1))
					{
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_tcscpy(szItem,pChild->xml);
						item_to_add = szItem;
					}	// if (i == (number-1))
				}	// if (pChild)
			}	// for (i = 0;i < pChildList->length;i++)
			return TRUE;
		}	// if (pChildList != NULL && number <= pChildList->length)
  }	// if (pXMLDoc != NULL)
  return FALSE;
}

// Setup ShellData tag, depending on TreeList data; 060505 p�d
/*
EXEC_ATTR1			= "visible";
EXEC_ATTR2			= "index";
EXEC_ATTR3			= "func";
EXEC_ATTR4			= "suite";
EXEC_ATTR5			= "strid";
EXEC_ATTR6			= "caption";
*/
BOOL XMLHandler::setupShellDataInXMLFile(vecTreeList& vec)
{

	CString S;
	UINT nSize = (UINT)vec.size();
	long i;
	int nNextLevel = -1;
	BOOL bIsMainItem;
	TCHAR szItem[128];
	MSXML2::IXMLDOMElementPtr pRootNode = pXMLDoc->documentElement;
	MSXML2::IXMLDOMNodeListPtr pChildList = pRootNode->getElementsByTagName(TREE_TAG);
  MSXML2::IXMLDOMAttributePtr pAttrib = NULL;
	MSXML2::IXMLDOMNodePtr pRoot = NULL;
	MSXML2::IXMLDOMNodePtr pNodeLevel1 = NULL;
	MSXML2::IXMLDOMNodePtr pNodeLevel2 = NULL;
	MSXML2::IXMLDOMNodePtr pNodeLevel3 = NULL;
	MSXML2::IXMLDOMNodePtr pNodeLevel4 = NULL;
	
	m_bIsThisShortcuts = (_tcscmp(extractFileName(m_sModulePath),SHELLDATA_ShortcutsFN) == 0);

	if (pChildList->length > 0)
	{
		// Remove all childern in ShellData tag; 060505 p�d
		for (i = 0;i < pChildList->length;i++)
		{
			delChild(1);
		}

		// Start adding ShellData elements as set in vector (vecTreeList); 060505 p�d
		pRoot = pRootNode->selectSingleNode(SHELLTREE_ID);
		for (UINT j = 0;j < nSize;j++)
		{
			if (pXMLDoc != NULL)
			{
				CTreeList rec = vec[j];
				// Check if level set in vec doesn't match next level; 061213 p�d
				if (nNextLevel > -1 && rec.getLevel() > nNextLevel)
				{
					rec.setLevel(nNextLevel);
				}
				if (pRoot)
				{
	
					// Check if there's any SubItem's. If so change them to MainItem; 060911 p�d
					if (_tcscmp(rec.getNodeName(),SUB_ITEM1) == 0 ||
						  _tcscmp(rec.getNodeName(),SUB_ITEM2) == 0 ||
						  _tcscmp(rec.getNodeName(),SUB_ITEM3) == 0 ||
						  _tcscmp(rec.getNodeName(),SUB_ITEM4) == 0)
					{
						rec.setNodeName(MAIN_ITEM);
						bIsMainItem = TRUE;
					}
					// If there's no Nodelevel1, we'll assume
					// that there's only MEXEC_ITEMS. If so
					// we'll need to make this entry, if it's a
					// EXEC_ITEM to be a MEXEC_ITEM; 081024 p�d
					if (_tcscmp(rec.getNodeName(),EXEC_ITEM) == 0 && pNodeLevel1 == NULL)
					{
						rec.setNodeName(MEXEC_ITEM);
						bIsMainItem = TRUE;
						rec.setLevel(1);	// Force to level 1 (Root); 0810245 p�d
					}
					// If there's no Nodelevel1, we'll assume
					// that there's only MHELP_ITEMS. If so
					// we'll need to make this entry, if it's a
					// HELP_ITEM to be a MHELP_ITEM; 081024 p�d
					if (_tcscmp(rec.getNodeName(),HELP_ITEM) == 0 && pNodeLevel1 == NULL)
					{
						rec.setNodeName(MHELP_ITEM);
						bIsMainItem = TRUE;
						rec.setLevel(1);	// Force to level 1 (Root); 0810245 p�d
					}

					// Create node element
					MSXML2::IXMLDOMElementPtr pElem = pXMLDoc->createElement(rec.getNodeName());
					// Create and add attributes
					// Attribute: Visible
					pAttrib = pXMLDoc->createAttribute( EXEC_ATTR1 );	
					_stprintf(szItem,_T("%d"),rec.getVisible());
					pAttrib->text = (szItem);
					pElem->attributes->setNamedItem( pAttrib );

					// Only add these attributes to Execute or Report items; 060505 p�d
					if (_tcscmp(rec.getNodeName(),EXEC_ITEM) == 0)
					{
						bIsMainItem = FALSE;
						// Attribute: Index
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR2 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%d"),rec.getIndex());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );

						// Attribute: Function
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR3 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%s"),rec.getFunc());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );

						// Attribute: Suite
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR4 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%s"),rec.getSuite());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );
					}	// if (_tcscmp(rec.getNodeName(),EXEC_ITEM) == 0)

					if (_tcscmp(rec.getNodeName(),MEXEC_ITEM) == 0)
					{
						bIsMainItem = FALSE;
						// Attribute: Index
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR2 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%d"),rec.getIndex());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );

						// Attribute: Function
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR3 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%s"),rec.getFunc());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );

						// Attribute: Suite
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR4 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%s"),rec.getSuite());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );
					}	// if (_tcscmp(rec.getNodeName(),EXEC_ITEM) == 0)

					// Only add these attributes to Execute or Report items; 060505 p�d
					if (_tcscmp(rec.getNodeName(),MHELP_ITEM) == 0)
					{
						bIsMainItem = FALSE;
						// Attribute: Index
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR2 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%d"),rec.getIndex());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );

						// Attribute: Function
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR3 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%s"),rec.getFunc());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );

						// Attribute: Suite
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR4 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%s"),rec.getSuite());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );
					}	 // if (_tcscmp(rec.getNodeName(),HELP_ITEM) == 0)

					// Only add these attributes to Execute or Report items; 060505 p�d
					if (_tcscmp(rec.getNodeName(),REPORT_ITEM) == 0)
					{
						bIsMainItem = FALSE;
						// Attribute: Index
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR2 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%d"),rec.getIndex());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );

						// Attribute: Function
						pAttrib = pXMLDoc->createAttribute( REPORT_ATTR3 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%s"),rec.getFunc());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );

						// Attribute: Suite
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR4 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%s"),rec.getSuite());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );
					}	// if (_tcscmp(rec.getNodeName(),REPORT_ITEM) == 0)

					// Only add these attributes to Execute or Report items; 060505 p�d
					if (_tcscmp(rec.getNodeName(),HELP_ITEM) == 0)
					{
						bIsMainItem = FALSE;
						// Attribute: Index
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR2 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%d"),rec.getIndex());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );

						// Attribute: Function
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR3 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%s"),rec.getFunc());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );

						// Attribute: Suite
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR4 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%s"),rec.getSuite());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );
					}	 // if (_tcscmp(rec.getNodeName(),HELP_ITEM) == 0)
					// Only add these attributes to Execute or Report items; 060505 p�d
					if (_tcscmp(rec.getNodeName(),EXTERN_ITEM) == 0)
					{
						bIsMainItem = FALSE;
						// Attribute: Index
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR2 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%d"),rec.getIndex());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );

						// Attribute: Function
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR3 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%s"),rec.getFunc());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );

						// Attribute: Suite
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR4 );	
						// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
						// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
						memset(szItem, 0, sizeof(szItem));
						_stprintf(szItem,_T("%s"),rec.getSuite());
						pAttrib->text = (szItem);
						pElem->attributes->setNamedItem( pAttrib );
					}	// if (_tcscmp(rec.getNodeName(),EXTERN_ITEM) == 0)

					// Attribute: StrID
					pAttrib = pXMLDoc->createAttribute( EXEC_ATTR5 );	
					// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
					// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
					memset(szItem, 0, sizeof(szItem));
					_stprintf(szItem,_T("%d"),rec.getStrID());
					pAttrib->text = (szItem);
					pElem->attributes->setNamedItem( pAttrib );

					// Attribute: Caption (ItemText)
					pAttrib = pXMLDoc->createAttribute( EXEC_ATTR6 );	
					pAttrib->text = (rec.getItemText());
					pElem->attributes->setNamedItem( pAttrib );

					// Attribute: arg1
					if (wcslen(rec.getArg1()) > 0)
					{
						pAttrib = pXMLDoc->createAttribute( EXEC_ATTR7 );	
						pAttrib->text = (rec.getArg1());
						pElem->attributes->setNamedItem( pAttrib );
					}

					// Only handle 2 levels on Shortcuts; 081024 p�d
					if (m_bIsThisShortcuts)
					{
						if (rec.getLevel() == 1)
						{
							if (_tcscmp(rec.getNodeName(),MAIN_ITEM) == 0)
							{
								pNodeLevel1 = pRoot->appendChild(pElem);
								nNextLevel = 2;
							}
							// This is a ROOT level EXECUTION ITEM, and it
							// can't have SubItems, so no pNodeLevel1 set here; 081024 p�d
							if (_tcscmp(rec.getNodeName(),MEXEC_ITEM) == 0)
							{
								//pNodeLevel1 = pRoot->appendChild(pElem);
								pRoot->appendChild(pElem);
								//nNextLevel = 2;	No sublevel here; 081024 p�d
							}
							// This is a ROOT level EXECUTION ITEM, and it
							// can't have SubItems, so no pNodeLevel1 set here; 081024 p�d
							if (_tcscmp(rec.getNodeName(),MHELP_ITEM) == 0)
							{
								//pNodeLevel1 = pRoot->appendChild(pElem);
								pRoot->appendChild(pElem);
								//nNextLevel = 2;	No sublevel here; 081024 p�d
							}
							
						}
						else if (rec.getLevel() > 1)
						{
							if (pNodeLevel1 != NULL)
							{
								pNodeLevel1->appendChild(pElem);
								//nNextLevel = 3;
							}
						}
					}	// if (m_bIsThisShortcuts)
					else
					{
						if (rec.getLevel() == 1)
						{
							if (_tcscmp(rec.getNodeName(),MAIN_ITEM) == 0)
							{
								pNodeLevel1 = pRoot->appendChild(pElem);
								nNextLevel = 2;
							}
							// This is a ROOT level EXECUTION ITEM, and it
							// can't have SubItems, so no pNodeLevel1 set here; 081024 p�d
							if (_tcscmp(rec.getNodeName(),MEXEC_ITEM) == 0)
							{
								//pNodeLevel1 = pRoot->appendChild(pElem);
								pRoot->appendChild(pElem);
								//nNextLevel = 2;	No sublevel here; 081024 p�d
							}
							// This is a ROOT level EXECUTION ITEM, and it
							// can't have SubItems, so no pNodeLevel1 set here; 081024 p�d
							if (_tcscmp(rec.getNodeName(),MHELP_ITEM) == 0)
							{
								//pNodeLevel1 = pRoot->appendChild(pElem);
								pRoot->appendChild(pElem);
								//nNextLevel = 2;	No sublevel here; 081024 p�d
							}
							
						}
						else if (rec.getLevel() == 2)
						{
							if (pNodeLevel1 != NULL)
							{
								pNodeLevel2 = pNodeLevel1->appendChild(pElem);
								nNextLevel = 3;
							}
						}
						else if (rec.getLevel() == 3)
						{
							if (pNodeLevel2 != NULL)
							{		
								pNodeLevel3 = pNodeLevel2->appendChild(pElem);
								nNextLevel = 4;
							}
						}
						else if (rec.getLevel() == 4)
						{
							if (pNodeLevel3 != NULL)
							{		
								pNodeLevel4 = pNodeLevel3->appendChild(pElem);
								nNextLevel = 5;
							}
						}
						else if (rec.getLevel() == 5)
						{
							if (pNodeLevel4 != NULL)
							{		
								pNodeLevel4->appendChild(pElem);
							}
						}
					} // else (if (m_bIsThisShortcuts))
				} // if (pRoot)
			}	// if (pXMLDoc != NULL)
		} // for (i = 0;i < nSize;i++)
		return TRUE;
	}
	return FALSE;
}

BOOL XMLHandler::addShellDataFileToShortcuts(LPCTSTR file)
{
	CString sStrID;
	MSXML2::IXMLDOMNodePtr ptrNodeSuite = NULL;
	MSXML2::IXMLDOMNodePtr ptrNodeUM = NULL;
	MSXML2::IXMLDOMNodePtr ptrItem = NULL;

	// Get Root node
	MSXML2::IXMLDOMNodePtr ptrRoot = pXMLDoc->documentElement;
	
	MSXML2::IXMLDOMElementPtr ptrChild = ptrRoot->selectSingleNode( SUITES_TAG );
	if (ptrChild)
	{
		ptrItem = pXMLDoc->createElement( SUITES_ITEM );
		ptrItem->text = _bstr_t(file);
		ptrChild->appendChild( ptrItem );
	}

	return TRUE;
}


BOOL XMLHandler::delShellDataFileFromShortcuts(LPCTSTR file)
{
	CString sFileName;
	MSXML2::IXMLDOMNodePtr ptrNodeSuite = NULL;
	MSXML2::IXMLDOMNodePtr ptrNodeUM = NULL;
	MSXML2::IXMLDOMNodePtr ptrItem = NULL;

	sFileName = extractFileName(file);
	// Get Root node
	MSXML2::IXMLDOMNodePtr ptrRoot = pXMLDoc->documentElement;
	
	MSXML2::IXMLDOMNodeListPtr ptrChildList = ptrRoot->selectNodes( SUITES_TAG_WC );
	if (ptrChildList)
	{
		for (long i = 0;i < ptrChildList->length;i++)
		{
			MSXML2::IXMLDOMNodePtr ptrChild = ptrChildList->item[i];
			if (ptrChild)
			{
				if (sFileName.CompareNoCase(ptrChild->text) == 0)
				{
					ptrRoot->selectSingleNode(SUITES_TAG)->removeChild(ptrChild);
				}
			}
		}
	}

	return TRUE;
}


BOOL XMLHandler::getDBInfoFromAdministration(vecADMIN_INI_DATABASES &vec)
{
	CString sFileName;
	CString sName;
	CString sDSN;
	CString sClient;
	MSXML2::IXMLDOMNodePtr ptrNodeSuite = NULL;
	MSXML2::IXMLDOMNodePtr ptrNodeUM = NULL;
	MSXML2::IXMLDOMNodePtr ptrItem = NULL;

	// Get Root node
	MSXML2::IXMLDOMNodePtr ptrRoot = pXMLDoc->documentElement;
	
	MSXML2::IXMLDOMNodeListPtr ptrChildList = ptrRoot->selectNodes( ADMIN_SUITE_TAG_WC );
	if (ptrChildList)
	{
		for (long i = 0;i < ptrChildList->length;i++)
		{
			MSXML2::IXMLDOMNodePtr ptrChild = ptrChildList->item[i];
			if (ptrChild)
			{
				sName = (BSTR)ptrChild->selectSingleNode("name")->text;
				sDSN = (BSTR)ptrChild->selectSingleNode("dsn")->text;
				sClient = (BSTR)ptrChild->selectSingleNode("client")->text;
				vec.push_back(_admin_ini_databases(sName,_T(""),sDSN,sClient));
			}
		}
	}

	return TRUE;
}

/* 	PRIVATE METHODS  */

// Help function: check if a suite is already in the list; 051213 p�d
BOOL XMLHandler::IsInList(LPCTSTR suite_fn,vecTreeList &list)
{
	if (list.size() == 0)
		return FALSE;

	for (UINT j = 0;j < list.size();j++)
	{
		CTreeList tree;
		tree = list[j];
		if (_tcscmp(suite_fn,tree.getSuite()) == 0 && tree.getCheck() == A_VALUE)
			return TRUE;
	}
	return FALSE;
}


void XMLHandler::interpretShellData(IDispatch *pNode)
{
	short nVisible;
	short nCheck;
	int nStrID;
  int nIdx;
	TCHAR szCaption[127];
	TCHAR szIcon[50];
	TCHAR szNode[127];
	TCHAR szText[127];
	TCHAR szAttr[127];
	TCHAR szAttr_func[127];
	TCHAR szAttr_suite[127];
	TCHAR szAttr_caption[127];
	TCHAR szArg1[127];

	CString sBaseName;

  MSXML2::IXMLDOMNamedNodeMapPtr pAttributes = NULL;
  MSXML2::IXMLDOMNodePtr pAttr = NULL;
	// Get Child node to start from; 060815 p�d
	MSXML2::IXMLDOMNodePtr pChild = (MSXML2::IXMLDOMNodePtr)pNode;
	MSXML2::IXMLDOMNode *ptrNode;
	MSXML2::IXMLDOMNode *ptrParent;
	int nLevel = 0,nLastLevel = -1;
	
	if (pChild)
	{

			ptrNode = (MSXML2::IXMLDOMNode *)pNode;
			ptrNode->get_parentNode(&ptrParent);
			nLevel = -3;
			while (ptrParent != NULL)
			{
				ptrNode->get_parentNode(&ptrParent);
				ptrNode = ptrParent;
				nLevel++;
			}

			// Clear TCHAR items (Anders Gustafsson dubugging session 070305); 070306 p�d
			// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
			memset(szNode, 0, sizeof(szNode));
			memset(szIcon, 0, sizeof(szIcon));
			memset(szNode, 0, sizeof(szNode));
			memset(szText, 0, sizeof(szText));
			memset(szAttr, 0, sizeof(szAttr));
			memset(szAttr_func, 0, sizeof(szAttr_func));
			memset(szAttr_suite, 0, sizeof(szAttr_suite));
			memset(szAttr_caption, 0, sizeof(szAttr_caption));
			memset(szArg1, 0, sizeof(szArg1));

			// Check basename. If basename is empty
			// this points to a comment in the ShellData xml-file
			// i.e. <!-- Comment -->; 080929 p�d
			sBaseName = (LPCTSTR)pChild->GetbaseName();
			if (sBaseName.IsEmpty())
				return;

			if (_tcscmp((LPCTSTR)pChild->GetbaseName(),MAIN_ITEM) == 0)
			{
				if (nLevel == 1)					
					_tcscpy(szNode,MAIN_ITEM);
				if (nLevel == 2)					
					_tcscpy(szNode,SUB_ITEM1);
				if (nLevel == 3)					
					_tcscpy(szNode,SUB_ITEM2);
				if (nLevel == 4)					
					_tcscpy(szNode,SUB_ITEM3);
				if (nLevel == 5)					
					_tcscpy(szNode,SUB_ITEM4);
			}
			else if (_tcscmp((LPCTSTR)pChild->GetbaseName(),MEXEC_ITEM) == 0)
			{
				if (nLevel == 1)					
					_tcscpy(szNode,MEXEC_ITEM);
				if (nLevel == 2)					
					_tcscpy(szNode,SUB_ITEM1);
				if (nLevel == 3)					
					_tcscpy(szNode,SUB_ITEM2);
				if (nLevel == 4)					
					_tcscpy(szNode,SUB_ITEM3);
				if (nLevel == 5)					
					_tcscpy(szNode,SUB_ITEM4);
			}
			// Added 100630 p�d
			else if (_tcscmp((LPCTSTR)pChild->GetbaseName(),MHELP_ITEM) == 0)
			{
				if (nLevel == 1)					
					_tcscpy(szNode,MHELP_ITEM);
				if (nLevel == 2)					
					_tcscpy(szNode,SUB_ITEM1);
				if (nLevel == 3)					
					_tcscpy(szNode,SUB_ITEM2);
				if (nLevel == 4)					
					_tcscpy(szNode,SUB_ITEM3);
				if (nLevel == 5)					
					_tcscpy(szNode,SUB_ITEM4);
			}
			else
			{
				if (nLastLevel > nLevel)
					nLevel -= 1;
				_tcscpy(szNode,(LPCTSTR)pChild->GetbaseName());
			}

		if (MSXML2::NODE_ELEMENT == pChild->nodeType)
		{
			_tcscpy(szText,pChild->text);
			if (nLevel == 1 && _tcscmp(szNode,MEXEC_ITEM) != 0 && _tcscmp(szNode,MHELP_ITEM) != 0)
			{
				nVisible	= 1;	// Default set visible
				nIdx	= -1;
				// Get string id attribute; 051201 p�d
				pAttributes = pChild->attributes;
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR5));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr,pAttr->text);
					nStrID = _tstoi(szAttr);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR1));
				if (pAttr != NULL)
				{
					_tcscpy(szAttr,pAttr->text);
					nVisible = _tstoi(szAttr);
				}
				// This text can be used instread of text in StringID
				// if String id = -1; 060503 p�d
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR6));
				if (pAttr != NULL)
				{
					_tcscpy(szAttr_caption,pAttr->text);
				}

			}
			// Added 2008-09-04 P�D
			else if (nLevel == 1 && _tcscmp(szNode,MEXEC_ITEM) == 0)
			{
				nVisible	= 1;	// Default set visible
				nIdx	= -1;
				pAttributes = pChild->attributes;

				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR1));
				if (pAttr != NULL)
				{
					_tcscpy(szAttr,pAttr->text);
					nVisible = _tstoi(szAttr);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR2));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr,pAttr->text);
					nIdx = _tstoi(szAttr);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR3));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr_func,pAttr->text);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR4));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr_suite,pAttr->text);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR5));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr,pAttr->text);
					nStrID = _tstoi(szAttr);
				}
				// This text can be used instread of text in StringID
				// if String id = -1; 060503 p�d
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR6));
				if (pAttr != NULL)
				{
					_tcscpy(szAttr_caption,pAttr->text);
				}
				// Argument added to Shelldata 2012-06-21 P�D #3269
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR7));
				if (pAttr != NULL)
				{	
					_tcscpy(szArg1,pAttr->text);
				}

			}
			else if (_tcscmp(szNode,MHELP_ITEM) == 0)
			{
				pAttributes = pChild->attributes;
				pAttr = pAttributes->getNamedItem(_bstr_t(HELP_ATTR1));
				if (pAttr != NULL)
				{
					_tcscpy(szAttr,pAttr->text);
					nVisible = _tstoi(szAttr);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(HELP_ATTR2));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr,pAttr->text);
					nIdx = _tstoi(szAttr);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(HELP_ATTR3));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr_func,pAttr->text);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(HELP_ATTR4));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr,pAttr->text);
					nStrID = _tstoi(szAttr);
				}
				// This text can be used instread of text in StringID
				// if String id = -1; 060503 p�d
				pAttr = pAttributes->getNamedItem(_bstr_t(HELP_ATTR5));
				if (pAttr != NULL)
				{
					_tcscpy(szAttr_caption,pAttr->text);
				}
			}
			else if (_tcscmp(szNode,SUB_ITEM1) == 0 ||
							 _tcscmp(szNode,SUB_ITEM2) == 0 ||
							 _tcscmp(szNode,SUB_ITEM3) == 0 ||
							 _tcscmp(szNode,SUB_ITEM4) == 0)
			{
				nVisible	= 1;	// Default set visible
				nIdx	= -1;
				// Get string id attribute; 051201 p�d
				pAttributes = pChild->attributes;
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR5));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr,pAttr->text);
					nStrID = _tstoi(szAttr);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR1));
				if (pAttr != NULL)
				{
					_tcscpy(szAttr,pAttr->text);
					nVisible = _tstoi(szAttr);
				}
				// This text can be used instread of text in StringID
				// if String id = -1; 060503 p�d
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR6));
				if (pAttr != NULL)
				{
					_tcscpy(szAttr_caption,pAttr->text);
				}

			}
			else if (_tcscmp(szNode,EXEC_ITEM) == 0)
			{
				pAttributes = pChild->attributes;
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR1));
				if (pAttr != NULL)
				{
					_tcscpy(szAttr,pAttr->text);
					nVisible = _tstoi(szAttr);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR2));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr,pAttr->text);
					nIdx = _tstoi(szAttr);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR3));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr_func,pAttr->text);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR4));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr_suite,pAttr->text);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR5));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr,pAttr->text);
					nStrID = _tstoi(szAttr);
				}
				// This text can be used instread of text in StringID
				// if String id = -1; 060503 p�d
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR6));
				if (pAttr != NULL)
				{
					_tcscpy(szAttr_caption,pAttr->text);
				}
				// Argument added to Shelldata 2012-06-21 P�D #3269
				pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR7));
				if (pAttr != NULL)
				{	
					_tcscpy(szArg1,pAttr->text);
				}
			}
			else if (_tcscmp(szNode,REPORT_ITEM) == 0)
			{
				pAttributes = pChild->attributes;
				pAttr = pAttributes->getNamedItem(_bstr_t(REPORT_ATTR1));
				if (pAttr != NULL)
				{
					_tcscpy(szAttr,pAttr->text);
					nVisible = _tstoi(szAttr);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(REPORT_ATTR2));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr,pAttr->text);
					nIdx = _tstoi(szAttr);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(REPORT_ATTR3));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr_func,pAttr->text);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(REPORT_ATTR4));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr_suite,pAttr->text);
				}
					pAttr = pAttributes->getNamedItem(_bstr_t(REPORT_ATTR5));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr,pAttr->text);
					nStrID = _tstoi(szAttr);
				}
				// This text can be used instread of text in StringID
				// if String id = -1; 060503 p�d
				pAttr = pAttributes->getNamedItem(_bstr_t(REPORT_ATTR6));
				if (pAttr != NULL)
				{
					_tcscpy(szAttr_caption,pAttr->text);
				}
			}
			else if (_tcscmp(szNode,HELP_ITEM) == 0)
			{
				pAttributes = pChild->attributes;
				pAttr = pAttributes->getNamedItem(_bstr_t(HELP_ATTR1));
				if (pAttr != NULL)
				{
					_tcscpy(szAttr,pAttr->text);
					nVisible = _tstoi(szAttr);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(HELP_ATTR2));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr,pAttr->text);
					nIdx = _tstoi(szAttr);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(HELP_ATTR3));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr_func,pAttr->text);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(HELP_ATTR4));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr,pAttr->text);
					nStrID = _tstoi(szAttr);
				}
				// This text can be used instread of text in StringID
				// if String id = -1; 060503 p�d
				pAttr = pAttributes->getNamedItem(_bstr_t(HELP_ATTR5));
				if (pAttr != NULL)
				{
					_tcscpy(szAttr_caption,pAttr->text);
				}
			}
			else if (_tcscmp(szNode,EXTERN_ITEM) == 0)
			{
				pAttributes = pChild->attributes;
				pAttr = pAttributes->getNamedItem(_bstr_t(EXTERN_ATTR1));
				if (pAttr != NULL)
				{
					_tcscpy(szAttr,pAttr->text);
					nVisible = _tstoi(szAttr);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(EXTERN_ATTR2));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr,pAttr->text);
					nIdx = _tstoi(szAttr);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(EXTERN_ATTR3));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr_func,pAttr->text);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(EXTERN_ATTR4));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr_suite,pAttr->text);
				}
				pAttr = pAttributes->getNamedItem(_bstr_t(EXTERN_ATTR5));
				if (pAttr != NULL)
				{	
					_tcscpy(szAttr,pAttr->text);
					nStrID = _tstoi(szAttr);
				}
				// This text can be used instread of text in StringID
				// if String id = -1; 060503 p�d
				pAttr = pAttributes->getNamedItem(_bstr_t(EXTERN_ATTR6));
				if (pAttr != NULL)
				{
					_tcscpy(szAttr_caption,pAttr->text);
				}
			}
	 
			_tcscpy(szCaption,strid(SHELLTREE_ID,EXEC_ATTR5,EXEC_ATTR6));	// Bug #3312 EXEC_ATTR7 satt; �ndrat till EXEC_ATTR6; 20120920 p�d
			_tcscpy(szIcon,str(HEADER_ICON));

			if (m_nCounter == 0)
			{
				nCheck = A_VALUE;
			}
			else
			{
				nCheck = NO_VALUE;
			}

			m_vecTreeList_temp.push_back(CTreeList(szCaption,
																				 szIcon,
																				 szNode,
																				 szText,
																				 nVisible,
																				 nIdx,
																				 szAttr_func,
																				 szAttr_suite,
																				 nStrID,
																				 m_sModulePath,
																				 nCheck,
																				 m_nCounter+1,
																				 szAttr_caption,
																				 nLevel,
																				 m_sModulePath,
																				 szArg1));

			m_nCounter++;
		} // if (MSXML2::NODE_ELEMENT == pChild->nodeType)
		nLastLevel = nLevel;
	}	// if (pChild)

	for (int i = 0;i < pChild->GetchildNodes()->Getlength();i++)
	{
		interpretShellData(pChild->GetchildNodes()->Getitem(i).Detach());
	}

}

void XMLHandler::findNodeByCounter(IDispatch *pNode)
{
	TCHAR szNode[127];
	TCHAR szAttr[127];

	// Clear TCHAR items (Anders Gustafsson dubugging session 070305).
	// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
	memset(szNode, 0, sizeof(szNode));
	memset(szAttr, 0, sizeof(szAttr));

  MSXML2::IXMLDOMNamedNodeMapPtr pAttributes = NULL;
  MSXML2::IXMLDOMNodePtr pAttr = NULL;
	// Get Child node to start from; 060815 p�d
	MSXML2::IXMLDOMNodePtr pChild = (MSXML2::IXMLDOMNodePtr)pNode;
	MSXML2::IXMLDOMNode *ptrNode;
	MSXML2::IXMLDOMNode *ptrParent;
	
	if (pChild)
	{
		m_nFindNodeNumCounter++;
		if (m_nFindNodeNumCounter == m_nFindNodeNum)
		{

				ptrNode = (MSXML2::IXMLDOMNode *)pNode;
				ptrNode->get_parentNode(&ptrParent);

				_tcscpy(szNode,(LPCTSTR)pChild->GetbaseName());

				if (MSXML2::NODE_ELEMENT == pChild->nodeType)
				{
					if (_tcscmp(szNode,EXEC_ITEM) == 0)
					{
						pAttributes = pChild->attributes;
						pAttr = pAttributes->getNamedItem(_bstr_t(EXEC_ATTR1));
						if (pAttr != NULL)
						{
							pAttr->text = (m_bShowItem ? "1" : "0");
						}
					}
					else if (_tcscmp(szNode,REPORT_ITEM) == 0)
					{
						pAttributes = pChild->attributes;
						pAttr = pAttributes->getNamedItem(_bstr_t(REPORT_ATTR1));
						if (pAttr != NULL)
						{
							pAttr->text = (m_bShowItem ? "1" : "0");
						}
					}
					else if (_tcscmp(szNode,HELP_ITEM) == 0)
					{
						pAttributes = pChild->attributes;
						pAttr = pAttributes->getNamedItem(_bstr_t(HELP_ATTR1));
						if (pAttr != NULL)
						{
							pAttr->text = (m_bShowItem ? "1" : "0");
						}
					}
					else if (_tcscmp(szNode,EXTERN_ITEM) == 0)
					{
						pAttributes = pChild->attributes;
						pAttr = pAttributes->getNamedItem(_bstr_t(EXTERN_ATTR1));
						if (pAttr != NULL)
						{
							pAttr->text = (m_bShowItem ? "1" : "0");
						}
					}
			} // if (MSXML2::NODE_ELEMENT == pChild->nodeType)
		}	// if (m_nFindNodeNumCounter == m_nFindNodeNum)
	}	// if (pChild)

	for (int i = 0;i < pChild->GetchildNodes()->Getlength();i++)
	{
		findNodeByCounter(pChild->GetchildNodes()->Getitem(i).Detach());
	}

}

