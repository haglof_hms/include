#if !defined(AFX_XMLPATHS_H)
#define AFX_XMLPATHS_H

////////////////////////////////////////////////////////////////////////////
// Tags in file ShellData files section UserModules; 051130 p�d
//
const LPCTSTR SUITES_TAG_WC	= _T("SuitesInShellTree/*");

const LPCTSTR SUITES_TAG			= _T("SuitesInShellTree");

const LPCTSTR SUITES_ITEM		= _T("name");

////////////////////////////////////////////////////////////////////////////
// Tags in file ShellData files section UserModules; 051130 p�d
//
const LPCTSTR ADMIN_SUITE_TAG_WC	= _T("DBConnection/*");

////////////////////////////////////////////////////////////////////////////
// Tags in file ShellData files section ShellTreeData; 051130 p�d
//
const LPCTSTR TREE_TAG			= _T("ShellTreeData/*");
const LPCTSTR TREE_TAG1			= _T("ShellTreeData");

const LPCTSTR MAIN_VERSION	= _T("//HMSShell/Header/Version");

const LPCTSTR MAIN_ITEM			= _T("MainItem");

const LPCTSTR SUB_ITEM1			= _T("SubItem1");
const LPCTSTR SUB_ITEM2			= _T("SubItem2");
const LPCTSTR SUB_ITEM3			= _T("SubItem3");
const LPCTSTR SUB_ITEM4			= _T("SubItem4");

// Execute item: use these to execute an Item in a Suite/Usermodule
const LPCTSTR EXEC_ITEM			= _T("ExecItem");
const LPCTSTR MEXEC_ITEM		= _T("MExecItem");

const LPCTSTR EXEC_ATTR1			= _T("visible");
const LPCTSTR EXEC_ATTR2			= _T("index");
const LPCTSTR EXEC_ATTR3			= _T("func");
const LPCTSTR EXEC_ATTR4			= _T("suite");
const LPCTSTR EXEC_ATTR5			= _T("strid");
const LPCTSTR EXEC_ATTR6			= _T("caption");
const LPCTSTR EXEC_ATTR7			= _T("arg1");	// Added 2012-06-21 p�d #3269

// Report item: use these to open a report in the Report suite
const LPCTSTR REPORT_ITEM		= _T("ReportItem");

const LPCTSTR REPORT_ATTR1		= _T("visible");
const LPCTSTR REPORT_ATTR2		= _T("index");
const LPCTSTR REPORT_ATTR3		= _T("report");	// Name of a FastReport report extension .fr3
const LPCTSTR REPORT_ATTR4		= _T("suite");	// Name of Suite "Reports.dll"
const LPCTSTR REPORT_ATTR5		= _T("strid");
const LPCTSTR REPORT_ATTR6		= _T("caption");

// Report item: use these to open a report in the Report suite
const LPCTSTR HELP_ITEM			= _T("HelpItem");
const LPCTSTR MHELP_ITEM		= _T("MHelpItem");	// Added 100629 p�d

const LPCTSTR HELP_ATTR1			= _T("visible");
const LPCTSTR HELP_ATTR2			= _T("index");
const LPCTSTR HELP_ATTR3			= _T("func");	// Name of the helpfile to be displayed
const LPCTSTR HELP_ATTR4			= _T("strid");
const LPCTSTR HELP_ATTR5			= _T("caption");

// Report item: use these to open a report in the Report suite
const LPCTSTR EXTERN_ITEM		= _T("ExternItem");

const LPCTSTR EXTERN_ATTR1		= _T("visible");
const LPCTSTR EXTERN_ATTR2		= _T("index");
const LPCTSTR EXTERN_ATTR3		= _T("func");		// Name of the helpfile to be displayed
const LPCTSTR EXTERN_ATTR4		= _T("suite");	// Can be a serachpath to a external program
const LPCTSTR EXTERN_ATTR5		= _T("strid");
const LPCTSTR EXTERN_ATTR6		= _T("caption");

////////////////////////////////////////////////////////////////////////////
//	Tags for header data in ShellData xml-files; 051115 p�d
//
const LPCTSTR SHELLTREE_ID				= _T("//ShellTreeData");

const LPCTSTR HEADER_NAME				= _T("//Header/Name");
const LPCTSTR HEADER_VERSION			= _T("//Header/Version");
const LPCTSTR HEADER_ICON				= _T("//Header/Icon");
const LPCTSTR HEADER_AUTHOR			= _T("//Header/Author");
const LPCTSTR HEADER_STARTDATE		= _T("//Header/Startdate");

////////////////////////////////////////////////////////////////////////////
//	Tags for header data in HMSShell language xml-files; 051202 p�d
//
const LPCTSTR LANGFILE_HEADER_LANGABREV		= _T("//Header/LangAbrev");
const LPCTSTR LANGFILE_HEADER_LANGUAGE			= _T("//Header/Language");


#endif