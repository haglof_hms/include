#pragma once

int RemoveDecimalpoint(char* szBuf);
unsigned int CStringToInt(CString csSource);
float CStringToFloat(CString csSource);
void CStringToChar(char *dest, CString csSource);

int RenameINV(CString csFilename, CString csNewname);
int GetLangSettings(CString csInput, CString csDelimiter, CStringArray* pcaLangs);
