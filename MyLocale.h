#pragma once

class CLocale
{
private:

public:
	static CString GetLangString(CString csLang);
	CString GetLangString(int nIndex);
	CString GetLangAbbr(CString csLang);
	static CString FixLocale(CString csSource);
};