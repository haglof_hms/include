#pragma once

#include "ICollectionAccessor.h"
#include <afxtempl.h>

template<class TYPE, class ARG_TYPE>
class CListAccessor : public ICollectionAccessor<TYPE>
{
public:

	CListAccessor() { m_pList = NULL; }
	CListAccessor(CList<TYPE, ARG_TYPE> *pList)
	{
		SetCollection(pList);
		ASSERT(m_pList);
	}

	void SetCollection(CList<TYPE, ARG_TYPE> *pList)
	{
		m_pList = pList;
		ASSERT(m_pList);
	}

	TYPE GetAt(long nIndex)
	{
		ASSERT(m_pList);
		return m_pList->GetAt(m_pList->FindIndex(nIndex));
	}

	void Swap(long nFirst, long nSecond)
	{
		ASSERT(m_pList);
		POSITION posFirst = m_pList->FindIndex(nFirst);
		POSITION posSecond = m_pList->FindIndex(nSecond);
		TYPE typeFirst = m_pList->GetAt(posFirst);

		// set the item at the first position to equal the second:
		m_pList->SetAt(posFirst, m_pList->GetAt(posSecond));
		
		// now the second to equal the first:
		m_pList->SetAt(posSecond, typeFirst);
	}

	long GetCount()
	{
		ASSERT(m_pList);
		return (long)m_pList->GetCount();
	}

protected:
	CList<TYPE, ARG_TYPE> *m_pList;
};