// IComparer.h: interface for the IComparer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ICOMPARER_H__0E3F14C1_FA2D_4908_9847_5C58240DA757__INCLUDED_)
#define AFX_ICOMPARER_H__0E3F14C1_FA2D_4908_9847_5C58240DA757__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

template<class TYPE>
class IComparer
{
public:

	IComparer() {};
	virtual ~IComparer() {}

	void SetA(const TYPE &obj)
	{
		m_cmpA = obj;
	}
	void SetB(const TYPE &obj) 
	{
		m_cmpB = obj;
	}

	virtual bool IsABigger() const = 0;
	virtual bool IsBBigger() const = 0 ;
	virtual bool Equal() const = 0 ;

protected:
	TYPE m_cmpA;
	TYPE m_cmpB;
};

#endif // !defined(AFX_ICOMPARER_H__0E3F14C1_FA2D_4908_9847_5C58240DA757__INCLUDED_)
