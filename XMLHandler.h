#if !defined(AFX_XMLHANDLER_H)
#define AFX_XMLHANDLER_H

#include "XMLPaths.h"
#include "pad_hms_miscfunc.h"

#include <vector>

using namespace std;


//-------------------------------------------------------------------
//	This class is used in reading shelltree data from modules 
//	language files; 050324 p�d 
//-------------------------------------------------------------------
class CTreeList
{
private:
	TCHAR szCaption[50];
	TCHAR szIcon[50];
  TCHAR szNodeName[50];
  TCHAR szElement[50];
  short nVisible;
  int nIndex;
  TCHAR szFunc[50];
  TCHAR szSuite[64];
	int nStrID;
  TCHAR szSuitePath[MAX_PATH];
	short nCheck;	// A check value used e.g. when setting up Navigaton bar, values can be NO_VALUE or A_VALUE; 051124 p�d
	long nCounter;
	TCHAR szItemText[128];
	int nLevel;
	TCHAR szShellDataFile[MAX_PATH];
	TCHAR szArg1[128];
public:
  CTreeList()
  {
		_tcscpy_s(szCaption,50,_T(""));
		_tcscpy_s(szIcon,50,_T(""));
		_tcscpy_s(szNodeName,50,_T(""));
		_tcscpy_s(szElement,50,_T(""));
		nVisible		= 1;
		nIndex			= -1;
		_tcscpy_s(szFunc,50,_T(""));
		_tcscpy_s(szSuite,64,_T(""));
		nStrID			= -1;
		_tcscpy_s(szSuitePath,MAX_PATH,_T(""));
		nCheck			= -1;
		nCounter		= 0;
		_tcscpy_s(szItemText,128,_T(""));
		nLevel = -1;
		_tcscpy_s(szShellDataFile,MAX_PATH,_T(""));
		_tcscpy_s(szArg1,128,_T(""));
  }
  CTreeList(LPCTSTR caption,LPCTSTR icon,LPCTSTR node_name,LPCTSTR element,short visible,int idx,LPCTSTR func,
						LPCTSTR suite,int strid,LPCTSTR suite_path,short check,long counter,LPCTSTR item_text,int level,
						LPCTSTR shell_data_file,LPCTSTR arg1)
  {
		_tcscpy(szCaption,caption);
		_tcscpy(szIcon,icon);
		_tcscpy(szNodeName,node_name);
		_tcscpy(szElement,element);
		nVisible		= visible;
		nIndex			= idx;
		_tcscpy(szFunc,func);
		_tcscpy(szSuite,suite);
		nStrID			= strid;
		_tcscpy(szSuitePath,suite_path);
		nCheck			= check;
		nCounter		= counter;
		_tcscpy(szItemText,(item_text));
		nLevel = level;
		_tcscpy_s(szShellDataFile,MAX_PATH,(shell_data_file));
		_tcscpy(szArg1,arg1);
  }
  CTreeList(const CTreeList &c)
  {
		*this = c;
  }
	LPCTSTR getCaption()			{ return szCaption; }
	LPCTSTR getIcon()				{ return szIcon; }
	LPCTSTR getNodeName()	  { return szNodeName; }
	void setNodeName(LPCTSTR v)	{	_tcscpy(szNodeName,v); }
	LPCTSTR getElement()			{ return szElement; }
	short getVisible()			{ return nVisible; }
	void setVisible(short v) { nVisible = v;	}
	int getIndex()					{ return nIndex; }
	LPCTSTR getFunc()				{ return szFunc; }
	LPCTSTR getSuite()				{ return szSuite; }
	int getStrID()					{ return nStrID; }
	LPCTSTR getSuitePath()		{ return szSuitePath; }
	short getCheck()				{ return nCheck; }
	long getCounter()				{ return nCounter; }
	LPCTSTR getItemText()		{ return szItemText; }
	void setItemtext(LPCTSTR s) { _tcscpy(szItemText,s);	}
	int getLevel(void)			{ return nLevel; }
	void setLevel(int v)		{ nLevel = v;  }
	LPCTSTR getShellDataFile(void)	{ return szShellDataFile; }
	LPCTSTR getArg1()				{ return szArg1; }
};

typedef std::vector<CTreeList> vecTreeList;


// class handler for XML; 2005-03-24 p�d
class XMLHandler
{
private:  // Data members
	TCHAR buff[255];
	MSXML2::IXMLDOMDocument2Ptr pXMLDoc;
	vecTreeList treeData;
	CString m_sModulePath;

	BOOL m_bIsThisShortcuts;

private:  // Methods
	BOOL IsInList(LPCTSTR,vecTreeList &);

	BOOL delChild(long number);

	int m_nFindNodeNum;
	int m_nFindNodeNumCounter;
	BOOL m_bShowItem;
	void interpretShellData(IDispatch *pNode);
	void findNodeByCounter(IDispatch *pNode);

	vecTreeList m_vecTreeList_temp;

	int m_nCounter;

	CString m_sLangSet;

public:
	// Defualt constructor
	XMLHandler();
	// Constructor
	XMLHandler(LPCTSTR module_path,HINSTANCE hinst);
	// Destructor
	~XMLHandler();

	BOOL load(LPCTSTR xml_fn);
	BOOL save(LPCTSTR xml_fn);

	int getVersionNumber(void);

	BOOL getShellTreeData(vecTreeList &,BOOL clr = FALSE);

	BOOL getSuitesInShellTree(CStringArray &);
#ifdef SUITEADMINISTRATION
	BOOL setSuitesInShellTree(CStringArray &suites_list);
	BOOL getShellTreeInfo(vecShellTree &tree_list);
#endif

	CString str(LPCTSTR path);

	CString strid(LPCTSTR path,LPCTSTR attr1,LPCTSTR attr2);

	BOOL setVisible(long number,BOOL show);

	BOOL addMainItem(long number,CTreeList& rec,BOOL add_to_suite);
	BOOL updMainItem(long number, CTreeList& rec,BOOL do_update);
	BOOL delMainItem(vecTreeList& vec_orig,vecTreeList& vec_new,BOOL do_delete);

	BOOL getSelectedItemInXMLFile(int number,CString& item_to_add);

	BOOL setupShellDataInXMLFile(vecTreeList&);

	BOOL addShellDataFileToShortcuts(LPCTSTR);
	BOOL delShellDataFileFromShortcuts(LPCTSTR);

	BOOL getDBInfoFromAdministration(vecADMIN_INI_DATABASES &);
};

#define NO_VALUE	-9999
#define A_VALUE		9999

#endif