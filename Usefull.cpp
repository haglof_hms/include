#include "stdafx.h"
#include "Usefull.h"

int RemoveDecimalpoint(char* szBuf)
{
	for(int nLoop=0; nLoop<(int)strlen(szBuf); nLoop++)
	{
		if(szBuf[nLoop] == '.') szBuf[nLoop] = ',';
	}

	return 1;
}

unsigned int CStringToInt(CString csSource)
{
	unsigned int ch=0,sum=0;
	
	for(int nLoop=0; nLoop<csSource.GetLength(); nLoop++)
	{
		ch=(csSource.GetAt(nLoop))-'0';
		if(ch >= 0 && ch <= 10)
		{
			sum*=10;
			sum+=ch;
		}
	}
	return sum;
}

float CStringToFloat(CString csSource)
{
	float sum;

	TCHAR szBuf[255];
	_stprintf(szBuf, _T("%S"), csSource.GetBuffer(0));
	sum = (float)_tstof(szBuf);
	
	return sum;
}

void CStringToChar(char *dest, CString csSource)
{
	int nLoop;
	for(nLoop=0; nLoop<csSource.GetLength(); nLoop++)
	{
		dest[nLoop]=(char)csSource.GetAt(nLoop);
	}
	dest[nLoop]='\0';
}

int RenameINV(CString csFilename, CString csNewname)
{
	CFile fh;
	char *pszBuf, szTmpBuf[1024], *pToken;
	int nLength, nTmp=0;
	BOOL bOk = FALSE;
	int nPos = 0;
	int nTag1=0, nTag2=0;

	if( fh.Open(csFilename, CFile::modeRead, NULL) != 0)
	{
		nLength = fh.GetLength();
		if( (pszBuf = (char*)malloc(nLength+1)) != NULL)
		{
			fh.Read(pszBuf, nLength);
			fh.Close();

			// parse the INV-file
			for(int nLoop=0; nLoop<nLength; nLoop++)
			{
				if(pszBuf[nLoop] == '~')
				{
					szTmpBuf[nPos] = '\0';
					nTmp = nLoop - strlen(szTmpBuf);
					nPos = 0;

					pToken = strtok(szTmpBuf, " ");
					if(pToken != NULL)
					{
#ifdef UNICODE
						USES_CONVERSION;
						nTag1 = _tstoi(A2W(pToken));
#else
						nTag1 = atoi(pToken);
#endif

						pToken = strtok(NULL, " \n\r");
						if(pToken != NULL)
						{
#ifdef UNICODE
							USES_CONVERSION;
							nTag2 = _tstoi(A2W(pToken));
#else
							nTag2 = atoi(pToken);
#endif

							if(nTag1 == 2 && nTag2 == 1)	// name
							{
								// save first part of the file
								if( fh.Open(csFilename, CFile::modeWrite|CFile::modeCreate, NULL) != 0)
								{
									fh.Write(pszBuf, nTmp); //nLoop - strlen(szTmpBuf));

									// our changed one
									sprintf(szTmpBuf, "2 1 \n%s", csNewname);
									fh.Write(szTmpBuf, strlen(szTmpBuf));

									// the rest
									pszBuf += nLoop;
									fh.Write(pszBuf, nLength - nLoop);

									fh.Close();
								}


								break;
							}
						}
					}
				}
				else
				{
					szTmpBuf[nPos] = pszBuf[nLoop];
					nPos++;
				}
			}

			bOk = TRUE;

			free(pszBuf);
		}
		else
		{
			fh.Close();
		}

	}

	return bOk;
}

int GetLangSettings(CString csInput, CString csDelimiter, CStringArray* pcaLangs)
{
	// split strings into a array
	int nStart = 0, nStop = 0;
	while( (nStop = csInput.Find(csDelimiter, nStart)) != -1 )
	{
		pcaLangs->Add( csInput.Mid(nStart, nStop-nStart) );

		nStart = nStop+1;
	}

	return TRUE;
}
