#pragma once

#include "ICollectionAccessor.h"
#include <afxtempl.h>

template<class TYPE, class ARG_TYPE>
class CArrayAccessor : public ICollectionAccessor<TYPE>
{
public:

	CArrayAccessor() { m_pArray = NULL; }
	CArrayAccessor(CArray<TYPE, ARG_TYPE> *pArray)
	{
		SetCollection(pArray);
		ASSERT(m_pArray);
	}

	void SetCollection(CArray<TYPE, ARG_TYPE> *pArray)
	{
		m_pArray = pArray;
		ASSERT(m_pArray);
	}

	TYPE GetAt(long nIndex)
	{
		ASSERT(m_pArray);
		return m_pArray->GetAt(nIndex);
	}

	void Swap(long nFirst, long nSecond)
	{
		ASSERT(m_pArray);
		TYPE typeFirst = m_pArray->GetAt(nFirst);

		// set the item at the first position to equal the second:
		m_pArray->SetAt(nFirst, m_pArray->GetAt(nSecond));
		
		// now the second to equal the first:
		m_pArray->SetAt(nSecond, typeFirst);
	}

	long GetCount()
	{
		ASSERT(m_pArray);
		return m_pArray->GetSize();
	}

protected:
	CArray<TYPE, ARG_TYPE> *m_pArray;
};