#include "stdafx.h"
#include "MyLocale.h"
#include <locale.h>

struct LANGDEF
{
	CString csTLA;
	WORD wLangID;
};

const LANGDEF languages[] =
{
	{ _T("ARA"),         MAKELANGID(LANG_ARABIC, SUBLANG_ARABIC_SAUDI_ARABIA) },
	{ _T("CHS"),    MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_SIMPLIFIED) },
	{ _T("CHT"), MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_TRADITIONAL) },
	{ _T("HRV"),       MAKELANGID(LANG_CROATIAN, SUBLANG_DEFAULT) },
	{ _T("CSY"),          MAKELANGID(LANG_CZECH, SUBLANG_DEFAULT) },
	{ _T("DAN"),         MAKELANGID(LANG_DANISH, SUBLANG_DEFAULT) },
	{ _T("NLD"),          MAKELANGID(LANG_DUTCH, SUBLANG_DUTCH) },
	{ _T("ENU"),        MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US) },
	{ _T("ETI"),       MAKELANGID(LANG_ESTONIAN, SUBLANG_DEFAULT) },
	{ _T("FIN"),        MAKELANGID(LANG_FINNISH, SUBLANG_DEFAULT) },
	{ _T("FRA"),         MAKELANGID(LANG_FRENCH, SUBLANG_FRENCH) },
	{ _T("DEU"),         MAKELANGID(LANG_GERMAN, SUBLANG_GERMAN) },
	{ _T("ELL"),          MAKELANGID(LANG_GREEK, SUBLANG_DEFAULT) },
	{ _T("HEB"),         MAKELANGID(LANG_HEBREW, SUBLANG_DEFAULT) },
	{ _T("HUN"),      MAKELANGID(LANG_HUNGARIAN, SUBLANG_DEFAULT) },
	{ _T("ITA"),        MAKELANGID(LANG_ITALIAN, SUBLANG_ITALIAN) },
	{ _T("JPN"),       MAKELANGID(LANG_JAPANESE, SUBLANG_DEFAULT) },
	{ _T("KOR"),         MAKELANGID(LANG_KOREAN, SUBLANG_DEFAULT) },
	{ _T("LVI"),        MAKELANGID(LANG_LATVIAN, SUBLANG_DEFAULT) },
	{ _T("LTH"),     MAKELANGID(LANG_LITHUANIAN, SUBLANG_LITHUANIAN) },
	{ _T("NOR"),      MAKELANGID(LANG_NORWEGIAN, SUBLANG_NORWEGIAN_BOKMAL) },
	{ _T("PLK"),         MAKELANGID(LANG_POLISH,  SUBLANG_DEFAULT) },
	{ _T("PTB"),  MAKELANGID(LANG_PORTUGUESE, SUBLANG_PORTUGUESE_BRAZILIAN) },
	{ _T("PTG"),     MAKELANGID(LANG_PORTUGUESE, SUBLANG_PORTUGUESE) },
	{ _T("ROM"),       MAKELANGID(LANG_ROMANIAN, SUBLANG_DEFAULT) },
	{ _T("RUS"),        MAKELANGID(LANG_RUSSIAN, SUBLANG_DEFAULT) },
	{ _T("SKY"),         MAKELANGID(LANG_SLOVAK, SUBLANG_DEFAULT) },
	{ _T("SLV"),      MAKELANGID(LANG_SLOVENIAN, SUBLANG_DEFAULT) },
	{ _T("ESP"),        MAKELANGID(LANG_SPANISH, SUBLANG_SPANISH_MODERN) },
	{ _T("SVE"),        MAKELANGID(LANG_SWEDISH, SUBLANG_DEFAULT) },
	{ _T("THA"),           MAKELANGID(LANG_THAI, SUBLANG_DEFAULT) },
	{ _T("UKR"),      MAKELANGID(LANG_UKRAINIAN, SUBLANG_DEFAULT) },
	{ _T("SRL"), MAKELANGID(LANG_SERBIAN, SUBLANG_SERBIAN_LATIN) },
	{ _T("SRB"), MAKELANGID(LANG_SERBIAN, SUBLANG_SERBIAN_CYRILLIC) },
};

// get the languagename in the current locale language
CString CLocale::GetLangString(CString csLang)
{
	CString csName = _T("");

	for(int nLoop=0; nLoop<sizeof(languages)/sizeof(LANGDEF); nLoop++)
	{
		if(languages[nLoop].csTLA == csLang)
		{
			TCHAR LocSig[512];
			GetLocaleInfo(languages[nLoop].wLangID, LOCALE_SLANGUAGE, (LPTSTR)&LocSig, sizeof(LocSig) / sizeof(TCHAR));
			csName = LocSig;
			break;
		}
	}
	
	return csName;
}

CString CLocale::GetLangAbbr(CString csLang)
{
	CString csName = _T("");

	for(int nLoop=0; nLoop<sizeof(languages)/sizeof(LANGDEF); nLoop++)
	{
		TCHAR LocSig[512];
		GetLocaleInfo(languages[nLoop].wLangID, LOCALE_SLANGUAGE, (LPTSTR)&LocSig, sizeof(LocSig) / sizeof(TCHAR));

		if(LocSig == csLang)
		{
			csName = languages[nLoop].csTLA;
			break;
		}
	}
	
	return csName;
}

CString CLocale::GetLangString(int nIndex)
{
	CString csName;
	TCHAR LocSig[512];

	if(nIndex > sizeof(languages)/sizeof(LANGDEF)) return _T("");

	GetLocaleInfo(languages[nIndex].wLangID, LOCALE_SLANGUAGE, (LPTSTR)&LocSig, sizeof(LocSig) / sizeof(TCHAR));
	csName = LocSig;

	return csName;
}

CString CLocale::FixLocale(CString csSource)
{
	CString csBuf;
	char cChar;

	csBuf = csSource;

	lconv* lc = localeconv();

	int nLoop = 0;
	for(nLoop=0; nLoop<csBuf.GetLength(); nLoop++)
	{
		if(csBuf.GetAt(nLoop) == ',')
		{
			cChar = ',';
			break;
		}
		else if(csBuf.GetAt(nLoop) == '.')
		{
			cChar = '.';
			break;
		}
	}

	if(nLoop != csBuf.GetLength())
	{
		if(strcmp(&cChar, lc->decimal_point) != 0)
		{
			csBuf.SetAt(nLoop, *lc->decimal_point);
		}
	}


	return csBuf;
}