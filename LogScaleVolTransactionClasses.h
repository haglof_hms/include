#if !defined(AFX_LOGSCALEVOLUMETRANSACTIONCLASSES_H)
#define AFX_LOGSCALEVOLUMETRANSACTIONCLASSES_H

#include <vector>


#define MM2IN 0.0393700787F // MM to IN
#define IN2MM 1.0/MM2IN // IN to MM
#define CM2IN 0.393700787F // CM to IN
#define IN2CM 1.0/CM2IN // IN to CM
#define CMV2CFV 35.31466672F  // Cubic M to Cubic FT
#define CFV2CMV 1.0/CMV2CFV // Cubic FT to Cubic M
#define DM2FT 0.328083990F // DM to FT
#define FT2DM 1.0/DM2FT // FT to DM
#define CM2FT 0.0328083990F // DM to FT
#define FT2CM 1.0/CM2FT // FT to DM
#define KG2LB 2.204622622 // KG to LB
#define LB2KG 1.0/KG2LB // LB to KG

#define CMV2CFV 35.31466672F  // Cubic M to Cubic FT
#define CFV2CMV 1.0/CMV2CFV // Cubic FT to Cubic M

#define DM2FT 0.328083990F // DM to FT
#define FT2DM 1.0/DM2FT // FT to DM

#define KG2LB 2.204622622 // KG to LB
#define LB2KG 1.0/KG2LB // LB to KG

#define FT2IN	12.0			// 1 Feet = 12 Inch
#define IN2FT 1.0/FT2IN		// Inch to Feet

#define FT2M	0.3048		// 1 Feet = 0.3048 m
#define M2FT	1/FT2M		// Meter to feet


//////////////////////////////////////////////////////////////////////////////////////////
// Volume indexes 
// OBS! �NDRA INTE ORDNINGEN P� INDEXERING AV VOLYMSFUNKTIONERNA
// DETTA F�R ATT KLAVEN ANV�NDER DESSA INDEX!!!!!!!!
const int ID_INT14				=	1;
const int ID_INT18				=	2;
const int ID_DOYLE				=	4;			// Doyle; skapare av formel
//const int ID_KNAUF				=	6;		// Knauf; skapare av formel
const int ID_BRUCE_SHUM	=	5;			// Bruce Shumacher; skapare av formel
const int ID_SCRIB_W			=	6;			// Scribner West-side; skapare av formel
//const int ID_SCRIB_E			=	7;		// Scribner East-side; skapare av formel
const int ID_JAS					=	8;			// JAS; skapare av formel
const int ID_HUBER				=	9;			// HAUBER; skapare av formel
const int ID_HUBER_TR		=	10;			// HAUBER turkiet
const int ID_GB_SCALE		=	14;				// GB Scale (Guo Biao)
const int ID_CFIB				=	11;			// Cubic feet, inside bark
const int ID_CFIB_100		=	12;			// Cubic feet, inside bark divided by 100.0
const int ID_CFOB				=	13;			// Cubic feet, on bark

class FUNC_INDEX
{
public:
	enum VOL_LENGTH_DIAM
	{
		HEADER = 1000,
		SPECIES = 1001,
		COL_HEADER = 1002,
		DATA = 1003,
		DATA2 = 1004
	};
};


//////////////////////////////////////////////////////////////////////////////////////////
// CLogs
class CLogs
{
//private:
	int pkLogID;
	int fkLoadID;							// Ticket
	CString sTagNumber;				// Tagbricka
	CString sSpcCode;
	CString sSpcName;
	int nFrequency;
	CString sGrade;
	double fDeduction;				// V�rde f�r reducering; procent el. abs. v�rde
	double fDOBTop;						// Toppdiamter �ver bark
	double fDIBTop;						// Toppdiamter under bark
	double fDOBRoot;					// Rotdiamter �ver bark
	double fDIBRoot;					// Rotdiamter under bark
	double fLengthMeas;				// Stockens inm�tta l�ngd
	double fLengthCalc;				// Stockens l�ngd ber�kning
	double fBark;							// Barkavdrag
	double fVolPricelist;
	double fLogPrice;					// Pris per stock
	int nVolFuncID;						// ID p� volymsfunktion
	CString sVolFuncName;			// Namn p� volymsfunktion; ex. Doyle etc.		
	//---------------------------------------------------------
	// Kustomf�lt f�r extra volymsfunktioner
	double fCustVol1;
	double fCustVol2;
	double fCustVol3;
	double fCustVol4;
	double fCustVol5;
	double fCustVol6;
	double fCustVol7;
	double fCustVol8;
	double fCustVol9;
	double fCustVol10;
	double fCustVol11;
	double fCustVol12;
	double fCustVol13;
	double fCustVol14;
	double fCustVol15;
	double fCustVol16;
	double fCustVol17;
	double fCustVol18;
	double fCustVol19;
	double fCustVol20;
	//---------------------------------------------------------
	// Egeninl�sta v�rden (styr �ver inm�tta JA)
	double fUserVolume;				// Volym
	double fUserToppDiam;			// Toppdiamter
	double fUserRootDiam;			// Rotdiamter
	double fUserLength;				// L�ngd
	double fUserPrice;				// Pris

	/*************************************************/
	// Added 2011-11-31; used for "korsklavning", scribner, JAS etc.
	double fDOBTop2;					// Toppdiamter �ver bark
	double fDIBTop2;					// Toppdiamter under bark
	double fDOBRoot2;					// Rotdiamter �ver bark
	double fDIBRoot2;					// Rotdiamter under bark
	double fUserToppDiam2;		// Toppdiamter
	double fUserRootDiam2;		// Rotdiamter
	/*************************************************/
	CString sReason;					// Reason (Anledning)
	CString sNotes;						// Notering
	CString m_sGISCoord1;
	CString m_sGISCoord2;
	/*************************************************/
	int nStatusFlag;					// Status f�r stocken; 0=I lager, 1=S�ld, 2=S�gverk (3=Kasserad,4=Saknas etc.)
	/*************************************************/
	int nFlag;
	CString dCreated;
	CString dUpdated;
	// Added 2011-10-05; used mainly on matching Tags
	CString sTicket;
	// Added 2012-02-15; holds specied id (key) from TicketSpc table
	int nSpcID;
	// Added 2012-03-05; holds origin of log; 0 = From a ticketfile, 1 = Manually entered
	int nTicketOrigin;


public:
	CLogs()
	{
		pkLogID = -1;
		fkLoadID = -1;							
		sTagNumber = L"";
		sSpcCode = L"";
		sSpcName = L"";
		nFrequency = -1;
		sGrade = L"";
		fDeduction = 0.0;
		fDOBTop = 0.0;						
		fDIBTop = 0.0;						
		fDOBRoot = 0.0;					
		fDIBRoot = 0.0;					
		fLengthMeas = 0.0;						
		fLengthCalc = 0.0;						
		fBark = 0.0;							
		fVolPricelist = 0.0;
		fLogPrice = 0.0;						
		nVolFuncID = -1;
		sVolFuncName = L"";
		//---------------------------------------------------------
		// Kustomf�lt f�r extra volymsfunktioner
		fCustVol1 = 0.0;
		fCustVol2 = 0.0;
		fCustVol3 = 0.0;
		fCustVol4 = 0.0;
		fCustVol5 = 0.0;
		fCustVol6 = 0.0;
		fCustVol7 = 0.0;
		fCustVol8 = 0.0;
		fCustVol9 = 0.0;
		fCustVol10 = 0.0;
		fCustVol11 = 0.0;
		fCustVol12 = 0.0;
		fCustVol13 = 0.0;
		fCustVol14 = 0.0;
		fCustVol15 = 0.0;
		fCustVol16 = 0.0;
		fCustVol17 = 0.0;
		fCustVol18 = 0.0;
		fCustVol19 = 0.0;
		fCustVol20 = 0.0;
		//---------------------------------------------------------
		fUserVolume = 0.0;			
		fUserToppDiam = 0.0;		
		fUserRootDiam = 0.0;		
		fUserLength = 0.0;			
		fUserPrice = 0.0;				
		sReason = L"";					
		sNotes = L"";						
		m_sGISCoord1 = L"";
		m_sGISCoord2 = L"";
		nStatusFlag = -1;
		nFlag = -1;
		nSpcID = -1;
		dCreated = L"";
		dUpdated = L"";
		sTicket = L"";
		nTicketOrigin = -1;


	}
	CLogs(int logid,int loadid,LPCTSTR tag_number,LPCTSTR spc_code,LPCTSTR spc_name,int frequency,LPCTSTR grade,double deduction,double dobtop,double dibtop,double dobroot,double dibroot,
				double length_meas,double length_calc,double bark,double vol_pricelist,double price,int vol_func_id,LPCTSTR vol_func_name,
			  double vol1,double vol2,double vol3,double vol4,double vol5,double vol6,double vol7,double vol8,double vol9,double vol10,
			  double vol11,double vol12,double vol13,double vol14,double vol15,double vol16,double vol17,double vol18,double vol19,double vol20,
				double uvol,double utopdia,double urootdia,double ulength,double uprice,double dobtop2,double dibtop2,double dobroot2,double dibroot2,double utopdia2,double urootdia2,LPCTSTR reason,
				LPCTSTR notes,LPCTSTR gis_coord1,LPCTSTR gis_coord2,int status_flag,int spc_id,int ticket_origin,int flag = -1,LPCTSTR created = L"",LPCTSTR updated = L"",LPCTSTR ticket = L"")
	{
		pkLogID = logid;
		fkLoadID = loadid;							
		sTagNumber = tag_number;
		sSpcCode = spc_code;
		sSpcName = spc_name;
		nFrequency = frequency;
		sGrade = grade;
		fDeduction = deduction;
		fDOBTop = dobtop;						
		fDIBTop = dibtop;						
		fDOBRoot = dobroot;					
		fDIBRoot = dibroot;					
		fLengthMeas = length_meas;						
		fLengthCalc = length_calc;						
		fBark = bark;							
		fVolPricelist = vol_pricelist;
		fLogPrice = price;						
		nVolFuncID = vol_func_id;
		sVolFuncName = vol_func_name;
		//---------------------------------------------------------
		// Kustomf�lt f�r extra volymsfunktioner
		fCustVol1 = vol1;
		fCustVol2 = vol2;
		fCustVol3 = vol3;
		fCustVol4 = vol4;
		fCustVol5 = vol5;
		fCustVol6 = vol6;
		fCustVol7 = vol7;
		fCustVol8 = vol8;
		fCustVol9 = vol9;
		fCustVol10 = vol10;
		fCustVol11 = vol11;
		fCustVol12 = vol12;
		fCustVol13 = vol13;
		fCustVol14 = vol14;
		fCustVol15 = vol15;
		fCustVol16 = vol16;
		fCustVol17 = vol17;
		fCustVol18 = vol18;
		fCustVol19 = vol19;
		fCustVol20 = vol20;
		//---------------------------------------------------------


		fUserVolume = uvol;			
		fUserToppDiam = utopdia;		
		fUserRootDiam = urootdia;		
		fUserLength = ulength;			
		fUserPrice = uprice;				

		fDOBTop2 = dobtop2;						
		fDIBTop2 = dibtop2;						
		fDOBRoot2 = dobroot2;					
		fDIBRoot2 = dibroot2;					
		fUserToppDiam2 = utopdia2;		
		fUserRootDiam2 = urootdia2;		

		sReason = reason;					
		sNotes = notes;					
		m_sGISCoord1 = gis_coord1;
		m_sGISCoord2 = gis_coord2;
		nStatusFlag = status_flag;
		nFlag = flag;
		nSpcID = spc_id;

		dCreated = created;
		dUpdated = updated;
		sTicket = ticket;
		nTicketOrigin = ticket_origin;

	}

	CLogs(const CLogs& c)
	{
		*this = c;
}

	// GET
	int getLogID() const { return pkLogID; }
	int getLoadID() const { return fkLoadID; }
	CString getTagNumber() const { return sTagNumber; }
	CString getSpcCode() const { return sSpcCode; }
	CString getSpcName() const { return sSpcName; }
	int getSpcID() const { return nSpcID; }
	int getFrequency() const { return nFrequency; }
	CString getGrade() const { return sGrade; }
	double getDeduction() const { return fDeduction; }
	double getDOBTop() const { return fDOBTop; }
	double getDIBTop() const { return fDIBTop; }
	double getDOBRoot() const { return fDOBRoot; }
	double getDIBRoot() const { return fDIBRoot; }
	double getLengthMeas() const { return fLengthMeas; }
	double getLengthCalc() const { return fLengthCalc; }
	double getBark() const { return fBark; }
	double getVolPricelist() const { return fVolPricelist; }
	double getLogPrice() const { return fLogPrice; }
	int getVolFuncID() const { return nVolFuncID; }
	CString getVolFuncName() const { return sVolFuncName; }
	//---------------------------------------------------------
	// Kustomf�lt f�r extra volymsfunktioner
	double getVolCust1() const { return fCustVol1; }
	double getVolCust2() const { return fCustVol2; }
	double getVolCust3() const { return fCustVol3; }
	double getVolCust4() const { return fCustVol4; }
	double getVolCust5() const { return fCustVol5; }
	double getVolCust6() const { return fCustVol6; }
	double getVolCust7() const { return fCustVol7; }
	double getVolCust8() const { return fCustVol8; }
	double getVolCust9() const { return fCustVol9; }
	double getVolCust10() const { return fCustVol10; }
	double getVolCust11() const { return fCustVol11; }
	double getVolCust12() const { return fCustVol12; }
	double getVolCust13() const { return fCustVol13; }
	double getVolCust14() const { return fCustVol14; }
	double getVolCust15() const { return fCustVol15; }
	double getVolCust16() const { return fCustVol16; }
	double getVolCust17() const { return fCustVol17; }
	double getVolCust18() const { return fCustVol18; }
	double getVolCust19() const { return fCustVol19; }
	double getVolCust20() const { return fCustVol20; }
	//---------------------------------------------------------

	double getUVol() const { return fUserVolume; }
	double getUTopDia() const { return fUserToppDiam; }
	double getURootDia() const { return fUserRootDiam; }
	double getULength() const { return fUserLength; }
	double getUPrice() const { return fUserPrice; }

	double getDOBTop2() const { return fDOBTop2; }
	double getDIBTop2() const { return fDIBTop2; }
	double getDOBRoot2() const { return fDOBRoot2; }
	double getDIBRoot2() const { return fDIBRoot2; }
	double getUTopDia2() const { return fUserToppDiam2; }
	double getURootDia2() const { return fUserRootDiam2; }

	CString getReason() const { return sReason; }
	CString getNotes() const { return sNotes; }
	CString getGISCoord1() const { return m_sGISCoord1; }
	CString getGISCoord2() const { return m_sGISCoord2; }
	int getStatusFlag() const { return nStatusFlag; }
	int getFlag() const { return nFlag; }
	CString getCreated() const { return dCreated; }
	CString getUpdated() const { return dUpdated; }
	CString getTicket() const { return sTicket; }
	int getTicketOrigin() const { return nTicketOrigin; }

	// SET
	void setLogID(int v) { pkLogID = v; }
	void setLoadID(int v) { fkLoadID = v; }
	void setTagNumber(LPCTSTR v) { sTagNumber = v; }
	void setSpcCode(LPCTSTR v) { sSpcCode = v; }
	void setSpcName(LPCTSTR v) { sSpcName = v; }
	void setSpcID(int v) { nSpcID = v; }
	void setFrequency(int v) { nFrequency = v; }
	void setGrade(LPCTSTR v) { sGrade = v; }
	void setDeduction(double v) { fDeduction = v; }
	void setDOBTop(double v) { fDOBTop = v; }
	void setDIBTop(double v) { fDIBTop = v; }
	void setDOBRoot(double v) { fDOBRoot = v; }
	void setDIBRoot(double v) { fDIBRoot = v; }
	void setLengthMeas(double v) { fLengthMeas = v; }
	void setLengthCalc(double v) { fLengthCalc = v; }
	void setBark(double v) { fBark = v; }
	void setVolPricelist(double v) { fVolPricelist = v; }
	void setLogPrice(double v) { fLogPrice = v; }
	void setVolFuncID(int v) { nVolFuncID = v; }
	void setVolFuncName(LPCTSTR v) { sVolFuncName = v; }
	//---------------------------------------------------------
	// Kustomf�lt f�r extra volymsfunktioner
	void setVolCust1(double v)	{ fCustVol1 = v; }
	void setVolCust2(double v)	{ fCustVol2 = v; }
	void setVolCust3(double v)	{ fCustVol3 = v; }
	void setVolCust4(double v)	{ fCustVol4 = v; }
	void setVolCust5(double v)	{ fCustVol5 = v; }
	void setVolCust6(double v)	{ fCustVol6 = v; }
	void setVolCust7(double v)	{ fCustVol7 = v; }
	void setVolCust8(double v)	{ fCustVol8 = v; }
	void setVolCust9(double v)	{ fCustVol9 = v; }
	void setVolCust10(double v)	{ fCustVol10 = v; }
	void setVolCust11(double v)	{ fCustVol11 = v; }
	void setVolCust12(double v)	{ fCustVol12 = v; }
	void setVolCust13(double v)	{ fCustVol13 = v; }
	void setVolCust14(double v)	{ fCustVol14 = v; }
	void setVolCust15(double v)	{ fCustVol15 = v; }
	void setVolCust16(double v)	{ fCustVol16 = v; }
	void setVolCust17(double v)	{ fCustVol17 = v; }
	void setVolCust18(double v)	{ fCustVol18 = v; }
	void setVolCust19(double v)	{ fCustVol19 = v; }
	void setVolCust20(double v)	{ fCustVol20 = v; }
	//---------------------------------------------------------

	void setUVol(double v) { fUserVolume = v; }
	void setUTopDia(double v) { fUserToppDiam = v; }
	void setURootDia(double v) { fUserRootDiam = v; }
	void setULength(double v) { fUserLength = v; }
	void setUPrice(double v) { fUserPrice = v; }

	void setDOBTop2(double v) { fDOBTop2 = v; }
	void setDIBTop2(double v) { fDIBTop2 = v; }
	void setDOBRoot2(double v) { fDOBRoot2 = v; }
	void setDIBRoot2(double v) { fDIBRoot2 = v; }
	void setUTopDia2(double v) { fUserToppDiam2 = v; }
	void setURootDia2(double v) { fUserRootDiam2 = v; }

	void setReason(LPCTSTR v) { sReason = v; }
	void setNotes(LPCTSTR v) { sNotes = v; }
	void setGISCoord1(LPCTSTR v) { m_sGISCoord1 = v; }
	void setGISCoord2(LPCTSTR v) { m_sGISCoord2 = v; }
	void setStatusFlag(int v) { nStatusFlag = v; }
	void setTicket(LPCTSTR v) { sTicket = v; }


	void setFlag(int v) { nFlag = v; }
	void setTicketOrigin(int v) { nTicketOrigin = v; }
};

//////////////////////////////////////////////////////////////////////////////////////////
// CUserVolTables
class CUserVolTables
{
//private:
	int pkID;
	int nFuncID;
	CString sFullName;	
	CString sAbbrevName;	
	CString sBasisName;	
	CString sCreatedBy;
	CString sDate;
	int nTableType;
	int nMeasuringMode;		// Inch or Metric
	CString sNotes;				// Notes for volume-table
	CString sVolTable;		// Actual semicolon-seperated volume-table
	int nLocked;					// Locked = 1, Unlocked = 0
	CString dCreated;			// created,
	CString dUpdated;			// last updated
public:
	CUserVolTables()
	{
		pkID = -1;
		nFuncID = -1;
		sFullName = L"";	
		sAbbrevName = L"";	
		sBasisName = L"";	
		sCreatedBy = L"";
		sDate = L"";
		nTableType = -1;
		nMeasuringMode = -1;
		sNotes = L"";
		sVolTable = L"";
		nLocked = 0;				// Locked = 1, Unlocked = 0
		dCreated = L"";			// created,
		dUpdated = L"";			// last updated
	}
	CUserVolTables(int pk_id,int func_id,LPCTSTR full_name,LPCTSTR abbrev_name,LPCTSTR basis_name,
								 LPCTSTR created_by,LPCTSTR date,int table_type,int measure_mode,
								 LPCTSTR notes,LPCTSTR vol_table,int locked,LPCTSTR created = L"",LPCTSTR updated = L"")
	{
		pkID = pk_id;
		nFuncID = func_id;
		sFullName = full_name;	
		sAbbrevName = abbrev_name;	
		sBasisName = basis_name;	
		sCreatedBy = created_by;
		sDate = date;
		nTableType = table_type;
		nMeasuringMode = measure_mode;
		sNotes = notes;
		sVolTable = vol_table;
		nLocked = locked;
		dCreated = created;			// created,
		dUpdated = updated;			// last updated
	}
	CUserVolTables(const CUserVolTables& c)
	{
		*this = c;
	}
	// GET
	int getID() const { return pkID; }
	int getFuncID() const { return nFuncID; }
	CString getFullName() const { return sFullName; }
	CString getAbbrevName() const { return sAbbrevName; }
	CString getBasisName() const { return sBasisName; }
	CString getCreatedBy() const { return sCreatedBy; }
	CString getDate() const { return sDate; }
	int getTableType() const { return nTableType; }
	int getMeasuringMode() const { return nMeasuringMode; }
	CString getNotes() const { return sNotes; }
	CString getVolTable() const { return sVolTable; }
	int getLocked() const { return nLocked; }
	CString getCreated() const { return dCreated; }
	CString getUpdated() const { return dUpdated; }			
	// SET
	void setFuncID(int v) { nFuncID = v; }
	void setFullName(LPCTSTR v) { sFullName = v; }
	void setAbbrevName(LPCTSTR v) { sAbbrevName = v; }
	void setBasisName(LPCTSTR v) { sBasisName = v; }
	void setCreatedBy(LPCTSTR v) { sCreatedBy = v; }
	void setDate(LPCTSTR v) { sDate = v; }
	void setTableType(int v) { nTableType = v; }
	void setMeasuringMode(int v) { nMeasuringMode = v; }
	void setNotes(LPCTSTR v) { sNotes = v; }
	void setVolTable(LPCTSTR v) { sVolTable = v; }
	void setLocked(int v) { nLocked = v; }
};


//////////////////////////////////////////////////////////////////////////////////////////
// CDefUserVolFuncs
class CDefUserVolFuncs
{
//private:
	int nFuncID;
	CString sFullName;	
	CString sAbbrevName;	
	CString sBasisName;	
public:
	CDefUserVolFuncs()
	{
		nFuncID = -1;
		sFullName = L"";	
		sAbbrevName = L"";	
		sBasisName = L"";	
	}
	CDefUserVolFuncs(int func_id,LPCTSTR full_name,LPCTSTR abbrev_name,LPCTSTR basis_name)
	{
		nFuncID = func_id;
		sFullName = full_name;	
		sAbbrevName = abbrev_name;	
		sBasisName = basis_name;	
	}
	CDefUserVolFuncs(const CDefUserVolFuncs& c)
	{
		*this = c;
	}
	// GET
	int getFuncID() const { return nFuncID; }
	CString getFullName() const { return sFullName; }
	CString getAbbrevName() const { return sAbbrevName; }
	CString getBasisName() const { return sBasisName; }
	// SET
	void setFuncID(int v) { nFuncID = v; }
	void setFullName(LPCTSTR v) { sFullName = v; }
	void setAbbrevName(LPCTSTR v) { sAbbrevName = v; }
	void setBasisName(LPCTSTR v) { sBasisName = v; }
};


typedef std::vector<CLogs> CVecLogs;
typedef std::vector<int> vecExtraVolFuncID;
typedef std::vector<CDefUserVolFuncs> vecDefUserVolFuncs;
typedef std::vector<CUserVolTables> CVecUserVolTables;

#endif