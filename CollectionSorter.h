// CollectionSorter.h: interface for the CCollectionSorter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COLLECTIONSORTER_H__05FCF147_A48C_4E76_B34F_6D4301B53748__INCLUDED_)
#define AFX_COLLECTIONSORTER_H__05FCF147_A48C_4E76_B34F_6D4301B53748__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>
#include "IComparer.h"
#include "DefaultComparer.h"
#include "DefaultPtrComparer.h"
#include "ICollectionAccessor.h"
#include "ListAccessor.h"
#include "ArrayAccessor.h"

template<class TYPE, class ARG_TYPE>
class CCollectionSorter
{
public:
	CCollectionSorter() {};
	virtual ~CCollectionSorter() {};

	void SortCollection(CList<TYPE, ARG_TYPE> &list, IComparer<ARG_TYPE> &cmp = CDefaultComparer<ARG_TYPE>())
	{
		CListAccessor<TYPE, ARG_TYPE> la(&list);
		Sort(0, la.GetCount() - 1, la, cmp);
	}
	
	void SortCollection(CArray<TYPE, ARG_TYPE> &array, IComparer<ARG_TYPE> &cmp = CDefaultComparer<ARG_TYPE>())
	{
		CArrayAccessor<TYPE, ARG_TYPE> aa(&array);
		Sort(0, aa.GetCount() - 1, aa, cmp);
	}

	void SortPointerCollection(CList<TYPE, ARG_TYPE> &list, IComparer<ARG_TYPE> &cmp = CDefaultPtrComparer<ARG_TYPE>())
	{
		CListAccessor<TYPE, ARG_TYPE> la(&list);
		Sort(0, la.GetCount() - 1, la, cmp);
	}
	
	void SortPointerCollection(CArray<TYPE, ARG_TYPE> &array, IComparer<ARG_TYPE> &cmp = CDefaultPtrComparer<ARG_TYPE>())
	{
		CArrayAccessor<TYPE, ARG_TYPE> aa(&array);
		Sort(0, aa.GetCount() - 1, aa, cmp);
	}

	void SortCollection(ICollectionAccessor<ARG_TYPE> &ca, IComparer<ARG_TYPE> &cmp = CDefaultComparer<ARG_TYPE>())
	{
		Sort(0, ca.GetCount() - 1, ca, cmp);
	}

protected:
	void Sort(long il, long ih, ICollectionAccessor<ARG_TYPE> &ca, IComparer<ARG_TYPE> &cmp)
	{
		long Lo = il;
		long Hi = ih;
		ARG_TYPE Mid = ca.GetAt((Lo + Hi) / 2);

		do
		{
			bool stop = false;

			cmp.SetB(Mid);
			
			// comma operator - last expression sued as test for while loop.
			while (cmp.SetA(ca.GetAt(Lo)), 
						cmp.IsBBigger())
			{
				Lo++;
			}
			while (cmp.SetA(ca.GetAt(Hi)), 
						cmp.IsABigger())
			{
				Hi--;
			}

			if (Lo <= Hi)
			{
				// swap elemtns:
				ca.Swap(Lo, Hi);
				Lo++;
				Hi--;
			}
		} while (Lo <= Hi);

		if (Hi > il) Sort(il, Hi, ca, cmp);
		if (Lo < ih) Sort(Lo, ih, ca, cmp);

		return;
	}
};

#endif // !defined(AFX_COLLECTIONSORTER_H__05FCF147_A48C_4E76_B34F_6D4301B53748__INCLUDED_)
