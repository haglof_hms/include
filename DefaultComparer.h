#pragma once

#include "IComparer.h"

template<class TYPE>
class CDefaultComparer : public IComparer<TYPE>
{
public:
	CDefaultComparer() {}
	virtual ~CDefaultComparer() {}

	bool IsABigger() const
	{
		return (m_cmpA > m_cmpB);
	}
	bool IsBBigger() const
	{
		return (m_cmpB > m_cmpA);
	}
	bool Equal() const
	{
		return (m_cmpA == m_cmpB);
	}
};
